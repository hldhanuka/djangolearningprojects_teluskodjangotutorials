from django.shortcuts import render
from .models import DropDownList

# Create your views here.
def index(request) :
    dpls = DropDownList.objects.all()

    return render(request, 'index.html', {'dpls' : dpls})