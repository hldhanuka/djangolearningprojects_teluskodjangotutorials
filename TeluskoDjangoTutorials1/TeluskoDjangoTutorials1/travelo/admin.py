from django.contrib import admin
from .models import DropDownList

# Register your models here.
admin.site.register(DropDownList)