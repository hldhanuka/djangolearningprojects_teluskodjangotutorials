from django.db import models

# Create your models here.
class DropDownList(models.Model):
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to='pics')
    is_show = models.BooleanField(default=False)
    price = models.IntegerField(default=0)
